// -----------------------------
lazy val root = (project in file("."))
  .settings(
    name := "jsoup-custom-element",
    version := "0.0.1",
    scalaVersion := "2.11.8",
    libraryDependencies ++= Seq(
      "com.typesafe" % "config" % "1.3.1",
      "commons-io" % "commons-io" % "2.5",
      "org.jsoup" % "jsoup" % "1.10.2"
    )
  )

package dto

import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import scala.collection.JavaConverters._

trait CustomElementLike {

  def getElement: org.jsoup.nodes.Element
  def copySelfBefore: CustomElementLike
  def copySelfAfter: CustomElementLike
  def scopeRoot: CustomElementLike
  def scopeBefore: CustomElementLike
  def scopeAfter: CustomElementLike
  def scopeClosestElement(cssQuery: String): CustomElementLike
  def scopeClosestElements[T](cssQuery: String)(nextFunction: Element => T): CustomElementLike
  def scopeClosestElement(cssQuery: String, matchTextContent: String): CustomElementLike
  def scopeParent: CustomElementLike
  def scopeClosestParent(tagName: String): CustomElementLike
  def replace(cssQuery: String, html: String): CustomElementLike
  def replace(cssQuery: String, arg: Element): CustomElementLike
  def replace(cssQuery: String, args: Seq[Element]): CustomElementLike
  def replaceAttr(cssQuery: String, attrMap:Map[String, String]): CustomElementLike
  def replaceAttr(cssQuery: String, attrName: String, attrValue:String): CustomElementLike
  def replaceAttrValue(attrName: String, targetAttrValue: String, replacementAttrValue: String): CustomElementLike
  def replaceAttrValue(attrName: String, targetAttrValue: String, replacementAttrValue: String, cssQuery: String): CustomElementLike
  def replaceTextContent(cssQuery: String): CustomElementLike
  def replaceTextContent(cssQuery: String, replacement: String): CustomElementLike
  def replaceElementAndOverwriteAttr(cssQuery: String, arg: Element): CustomElementLike
  def renameTag(tagName:String): CustomElementLike
  def remove: CustomElementLike
  def removeElement(cssQuery: String): CustomElementLike
  def removeAttr(attrName: String): CustomElementLike
  def removeAttrValue(attrName: String, targetAttrValue: String): CustomElementLike
  def removeAttrValue(attrName: String, targetAttrValue: String, cssQuery: String): CustomElementLike
  def removeClassAttrValueForDescendant(targetAttrValueList: Seq[String]): CustomElementLike
  def removeAttrSearched(attrName: String, cssQuery: String): CustomElementLike
  def getAttr: Map[String, String]
  def getAttr(attrName: String): Option[String]
  def setAttr(attrMap:Map[String, String]):CustomElementLike
  def getTextContent: Option[String]
  def setTextContent(text :String, cssQuery: String): CustomElementLike
  def appendAttr(attrName: String, attrValue: String): CustomElementLike
  def appendAttr(cssQuery: String, attrName: String, attrValue: String): CustomElementLike
  def addBefore(arg: Element): CustomElementLike
  def addBefore(arg: String): CustomElementLike
  def addBeforeSearched(cssQuery: String, arg: Element): CustomElementLike
  def addAfter(arg: Element): CustomElementLike
  def addAfter(arg: String): CustomElementLike
  def addAfterSearched(cssQuery: String, arg: Element): CustomElementLike
  def addAfterSearched(cssQuery: String, arg: String): CustomElementLike
  def addParent(arg: Element): CustomElementLike
  def addParent(arg: String): CustomElementLike
  def addChildFromLast(arg: Element): CustomElementLike
  def addChildFromFirst(arg: Element): CustomElementLike
  def addChildSelectedBefore(cssQuery: String, arg: Element): CustomElementLike
  def addChildSelectedAfter(cssQuery: String, arg: Element): CustomElementLike
  def addDescendantFromLast(cssQuery: String, arg: Element): CustomElementLike
  def addDescendantFromFirst(cssQuery: String, arg: Element): CustomElementLike

}

case class CustomElement(underlying: org.jsoup.nodes.Element) extends CustomElementLike {

  override def getElement: org.jsoup.nodes.Element = underlying
  override def copySelfBefore: CustomElement = {
    if(underlying != null && !underlying.toString.isEmpty) {
      val tagname = underlying.tagName()
      underlying.tagName("div")
      val newElement = Jsoup.parse(underlying.toString)
      underlying.before(newElement.body().child(0))
      underlying.tagName(tagname)
      val befoe = underlying.previousElementSibling()
      befoe.tagName(tagname)
    }
    CustomElement(underlying)
  }
  override def copySelfAfter: CustomElement = {
    if(underlying != null && !underlying.toString.isEmpty) {
      val tagname = underlying.tagName()
      underlying.tagName("div")
      val newElement = Jsoup.parse(underlying.toString)
      underlying.after(newElement.body().child(0))
      underlying.tagName(tagname)
      val next = underlying.nextElementSibling()
      next.tagName(tagname)
    }
    CustomElement(underlying)
  }
  override def scopeClosestElement(cssQuery: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      CustomElement(scopes.first())
    } else {
      CustomElement(underlying)
    }
  }
  override def scopeClosestElements[T](cssQuery: String)(nextFunction: Element => T): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      for (scope <- scopes.asScala) {
        nextFunction(scope)
      }
    }
    CustomElement(underlying)
  }
  override def scopeClosestElement(cssQuery: String, matchTextContent: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      if(scope.text() == matchTextContent) {
        return CustomElement(scope)
      }
    }
    CustomElement(underlying)
  }
  override def scopeParent: CustomElement = CustomElement(underlying.parent())
  override def scopeClosestParent(tagName: String): CustomElement = {
    for (scope <- underlying.parents().asScala) {
      if(scope.tagName() == tagName) {
        return CustomElement(scope)
      }
    }
    CustomElement(underlying)
  }
  override def scopeRoot: CustomElement = {
    val tmp = underlying.parents()
    if(!tmp.isEmpty) {
      CustomElement(tmp.last())
    } else {
      CustomElement(underlying)
    }
  }
  override def scopeBefore: CustomElement = {
    if(underlying.elementSiblingIndex() == 0) {
      CustomElement(underlying)
    } else {
      CustomElement(underlying.previousElementSibling())
    }
  }
  override def scopeAfter: CustomElement = {
    if(underlying.elementSiblingIndex() == 0) {
      CustomElement(underlying)
    } else {
      CustomElement(underlying.nextElementSibling())
    }
  }
  override def replace(cssQuery: String, html: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.before(html)
      scope.remove()
    }
    CustomElement(underlying)
  }
  override def replace(cssQuery: String, arg: Element): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      for (scope <- scopes.asScala) {
        scope.before(arg)
        scope.remove()
      }
    }
    CustomElement(underlying)
  }
  override def replace(cssQuery: String, args: Seq[Element]): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      for (scope <- scopes.asScala) {
        for(arg <- args) {
          scope.before(arg)
        }
        scope.remove()
      }
    }
    CustomElement(underlying)
  }
  override def replaceAttrValue(attrName: String, targetAttrValue: String, replacementAttrValue: String): CustomElement = {
    val tmpAttrValues: String = underlying.attr(attrName)
    val newAttrValues: String = tmpAttrValues.split(" ").map {
      case `targetAttrValue` => replacementAttrValue
      case x               => x
    }.mkString(" ")
    underlying.removeAttr(attrName)
    underlying.attr(attrName, newAttrValues.trim)
    CustomElement(underlying)
  }
  override def replaceAttrValue(attrName: String, targetAttrValue: String, replacementAttrValue: String, cssQuery: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      val tmpAttrValues: String = scope.attr(attrName)
      val newAttrValues: String = tmpAttrValues.split(" ").map {
        case `targetAttrValue` => replacementAttrValue
        case x               => x
      }.mkString(" ")
      scope.removeAttr(attrName)
      scope.attr(attrName, newAttrValues.trim)
    }
    CustomElement(underlying)
  }
  override def replaceAttr(cssQuery: String, attrName: String, attrValue:String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.attr(attrName,attrValue)
    }
    CustomElement(underlying)
  }
  override def replaceAttr(cssQuery: String, attrMap: Map[String, String]) :CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      for(attr <- attrMap) {
        scope.attr(attr._1,attr._2)
      }
    }
    CustomElement(underlying)
  }
  override def replaceTextContent(replacement:String): CustomElement = {
    underlying.text(replacement)
    CustomElement(underlying)
  }
  override def replaceTextContent(cssQuery: String, replacement:String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.text(replacement)
    }
    CustomElement(underlying)
  }
  override def replaceElementAndOverwriteAttr(cssQuery: String, arg: Element) :CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      for(attr <- scope.attributes().asScala) {
        arg.attr(attr.getKey, attr.getValue)
      }
      scope.before(arg)
      scope.remove()
    }
    CustomElement(underlying)
  }
  override def renameTag(tagName:String) : CustomElement = {
    underlying.tagName(tagName)
    CustomElement(underlying)
  }
  override def remove: CustomElement = {
    val self = underlying
    self.remove()
    CustomElement(underlying)
  }
  override def removeElement(cssQuery: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      for (scope <- scopes.asScala) {
        scope.remove()
      }
    }
    CustomElement(underlying)
  }
  override def removeAttr(attrName: String): CustomElement = {
    underlying.removeAttr(attrName)
    CustomElement(underlying)
  }
  override def removeAttrValue(attrName: String, targetAttrValue: String): CustomElement = {
    replaceAttrValue(attrName, targetAttrValue, "")
  }
  override def removeAttrValue(attrName: String, targetAttrValue: String, cssQuery: String): CustomElement = {
    replaceAttrValue(attrName, targetAttrValue, "", cssQuery)
  }
  override def removeClassAttrValueForDescendant(targetAttrValueList: Seq[String]): CustomElement = {
    targetAttrValueList.foreach { targetAttrValue =>
      replaceAttrValue("class", targetAttrValue, "", "." + targetAttrValue)
    }
    CustomElement(underlying)
  }
  override def removeAttrSearched(attrName: String, cssQuery: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.removeAttr(attrName)
    }
    CustomElement(underlying)
  }
  override def getAttr: Map[String, String] = {
    val attr = underlying.attributes()
    (for (kv <- attr.asScala) yield {
      kv.getKey -> kv.getValue
    }).toMap
  }
  override def getAttr(attrName: String): Option[String] = {
    val a = underlying.attr(attrName)
    if (a.isEmpty) None else Some(a)
  }
  override def getTextContent: Option[String] = {
    val a = underlying.text()
    if (a.isEmpty) None else Some(a)
  }
  override def setAttr(attrMap:Map[String, String]): CustomElement = {
    for(attr <- attrMap) {
      underlying.attr(attr._1,attr._2)
    }
    CustomElement(underlying)
  }
  override def setTextContent(text :String, cssQuery: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.text(text)
    }
    CustomElement(underlying)
  }
  def appendAttr(attrName: String, attrValue: String): CustomElement = {
    val attributes = underlying.attributes()
    val tmp: String = attributes.get(attrName)
    if (tmp.isEmpty) {
      attributes.put(attrName, attrValue)
    } else {
      attributes.put(attrName, tmp + " " + attrValue)
    }
    CustomElement(underlying)
  }
  def appendAttr(cssQuery: String, attrName: String, attrValue: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      val attributes = underlying.attributes()
      val tmp: String = attributes.get(attrName)
      if (tmp.isEmpty) {
        attributes.put(attrName, attrValue)
      } else {
        attributes.put(attrName, tmp + " " + attrValue)
      }
    }
    CustomElement(underlying)
  }
  override def addBefore(arg: Element): CustomElement = CustomElement(underlying.before(arg))
  override def addBefore(html: String): CustomElement = CustomElement(underlying.before(html))
  override def addBeforeSearched(cssQuery: String, arg: Element): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      for (scope <- scopes.asScala) {
        scope.before(arg)
      }
    }
    CustomElement(underlying)
  }
  override def addAfter(arg: Element): CustomElement = CustomElement(underlying.after(arg))
  override def addAfter(html: String): CustomElement = CustomElement(underlying.after(html))
  override def addAfterSearched(cssQuery: String, arg: Element): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    if (!scopes.isEmpty) {
      for (scope <- scopes.asScala) {
        scope.after(arg)
      }
    }
    CustomElement(underlying)
  }
  override def addAfterSearched(cssQuery: String, html: String): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.after(html)
    }
    CustomElement(underlying)
  }
  override def addParent(arg: String): CustomElement = {
    val parent = underlying.parent()
    if(parent != null && !parent.toString.isEmpty) {
      underlying.before(arg)
      val afterCopy = underlying.parent().child(underlying.elementSiblingIndex() - 1)
      afterCopy.appendChild(Jsoup.parse(underlying.toString).body().child(0))
      underlying.remove()
      CustomElement(afterCopy.child(0))
    } else {
      CustomElement(underlying)
    }
  }
  override def addParent(arg: Element): CustomElement = {
    val parent = underlying.parent()
    if(parent != null && !parent.toString.isEmpty) {
      underlying.before(arg)
      val afterCopy = underlying.parent().child(underlying.elementSiblingIndex() - 1)
      afterCopy.appendChild(Jsoup.parse(underlying.toString).body().child(0))
      underlying.remove()
      CustomElement(afterCopy.child(0))
    } else {
      CustomElement(underlying)
    }
  }
  override def addChildFromLast(arg: Element): CustomElement = {
    underlying.appendChild(arg)
    CustomElement(underlying)
  }

  override def addChildFromFirst(arg: Element): CustomElement = {
    underlying.prependChild(arg)
    CustomElement(underlying)
  }
  override def addChildSelectedBefore(cssQuery: String, arg: Element): CustomElement = {
    for (child <- underlying.children().asScala) {
      for (selected <- underlying.select(cssQuery).asScala) {
        if (child.equals(selected)) {
          child.before(arg)
        }
      }
    }
    CustomElement(underlying)
  }
  override def addChildSelectedAfter(cssQuery: String, arg: Element): CustomElement = {
    for (child <- underlying.children().asScala) {
      for (selected <- underlying.select(cssQuery).asScala) {
        if (child.equals(selected)) {
          child.after(arg)
        }
      }
    }
    CustomElement(underlying)
  }
  override def addDescendantFromFirst(cssQuery: String, arg: Element): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.prependChild(arg)
    }
    CustomElement(underlying)
  }
  override def addDescendantFromLast(cssQuery: String, arg: Element): CustomElement = {
    val scopes: Elements = underlying.select(cssQuery)
    for (scope <- scopes.asScala) {
      scope.appendChild(arg)
    }
    CustomElement(underlying)
  }
}

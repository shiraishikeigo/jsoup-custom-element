```scala
package example

import dto.CustomElement
import org.jsoup.Jsoup

object Example {

  val dummy =
    """
      |<!DOCTYPE html>
      |<html lang="en">
      |    <head>
      |        <meta charset="UTF-8">
      |        <title id="title">dummy</title>
      |    </head>
      |    <body>
      |      <div class="row">
      |          <p class="foo">hoge</p>
      |          <div class="foo col-md-12">
      |             <span class="baz" id="foobar">
      |                 huga
      |             </span>
      |             <div class="baz" id="foobar2">
      |               <ul>
      |                 <li>White whites</li>
      |                 <li>Lion</li>
      |                 <li>Black butter</li>
      |                 <li>Chimpanzee</li>
      |                 <li>Wildcat</li>
      |               </ul>
      |             </div>
      |          </div>
      |      </div>
      |      <div class="bar">
      |        <table>
      |          <tr id="tr-id">
      |            <td>CPU</td>
      |            <td>Pentium3 500MHz</td>
      |          </tr>
      |          <tr>
      |            <td>Memory</td>
      |            <td>128kB</td>
      |          </tr>
      |          <tr>
      |            <td>HDD</td>
      |            <td>10GB</td>
      |          </tr>
      |        </table>
      |      </div>
      |    </body>
      |</html>
    """.stripMargin

  def main(args: Array[String]): Unit = {

    val customElement = CustomElement(Jsoup.parse(dummy))

    print(
      customElement
        .replaceTextContent("#title", "William Shakespeare")
        .replace(cssQuery = "p.foo", """<span>To be, or not to be: that is the question.</span>""")
        .scopeClosestElement("span.baz").removeAttr("id")
        .scopeRoot
        .addDescendantFromLast(cssQuery = "tr",
          customElement.scopeClosestElement("#foobar2").renameTag("td").getElement)
        .scopeClosestElement(cssQuery = "td", matchTextContent = "Memory")
        .scopeClosestParent("tr")
        .addChildSelectedAfter(cssQuery = "td",
          customElement.scopeClosestElement("#foobar2").renameTag("td").removeAttr("id").getElement)
        .scopeRoot
        .scopeClosestElement("#tr-id")
        .addChildSelectedAfter("td",
          // please take care mutable
          CustomElement(Jsoup.parse(dummy)).scopeClosestElement("#foobar2").renameTag("td").getElement)
        .scopeRoot
    )
  }


}

```